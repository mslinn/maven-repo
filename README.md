# mslinn maven repository

Welcome to my public artifacts repository. You can use it with sbt, maven, ivy or another compatible dependency manager.

## To use with sbt
In build.sbt, add the following:

````
resolvers += "mslinn-bitbucket" at "https://bitbucket.org/mslinn/maven-repo/raw/master/"
````

## To use with maven

In your ```pom.xml```, add this snippet:
  
```
<repositories>
    <repository>
        <id>mslinn-bitbucket</id>
        <name>mslinn maven repository</name>
        <url>https://bitbucket.org/mslinn/maven-repo/raw/master/</url>
        <layout>default</layout>
    </repository>
</repositories>
```
  
## To use with ivy

Use a resolver like the following:

```
<ibiblio
    name="mslinn-bitbucket"
    root="https://bitbucket.org/mslinn/maven-repo/raw/master/"
    m2compatible="true" />
```

